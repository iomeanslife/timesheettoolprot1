extern crate chrono;

use chrono::Local;
use std::collections::hash_map::DefaultHasher;
use std::env;
use std::fs::OpenOptions;
use std::hash::{Hash, Hasher};
use std::io::Write;

fn main() {
    let added = determine_command();
    let mut file = OpenOptions::new()
        .append(true)
        .open("./res/data.txt")
        .expect("file opening/creation failed");

    file.write(added.0.as_bytes())
        .expect("write from literal string failed");
    if let Some(hash) = added.1 {
        println!("Hash has been set to \"{}\"", hash);
    }
}

fn determine_command() -> (String, Option<String>) {
    let args: Vec<String> = env::args().collect();
    let command: &str = &args[1];
    match command {
        "new" => {
            // UI info.
            println!("Create new");

            let parameter = &args[2][..];

            // Create hash.
            let mut hasher = DefaultHasher::new();
            Hash::hash_slice(parameter.as_bytes(), &mut hasher);
            let hash: String = hasher.finish().to_string();

            // Get current formated time.
            let time_now = Local::now();
            let formated_time = time_now.format("%Y-%m-%d.%H_%M");
            // Create result string.
            (
                format!(
                    "
[{}]
    [Task] = \"{}\"
    [Hash] = \"{}\"",
                    formated_time, parameter, hash
                ),
                Some(hash),
            )
        }
        "existing" => {
            // UI info.
            println!("Add to existing");

            // Retrive hash.
            let hash = &args[2][..];

            // Get current formated time.
            let time_now = Local::now();
            let formated_time = time_now.format("%Y-%m-%d.%H_%M");

            // Create result string.
            (
                format!(
                    "
[{}]
    [Hash] = \"{}\"",
                    formated_time, hash
                ),
                None,
            )
        }
        "future" => {
            println!("Create a future task");
            let parameter = &args[2][..];
            let mut hasher = DefaultHasher::new();
            Hash::hash_slice(parameter.as_bytes(), &mut hasher);
            let hash: String = hasher.finish().to_string();
            (
                format!(
                    "
[Future-{}]
    [Task] = \"{}\"
    [Hash] = \"{}\"",
                    hash, parameter, hash
                ),
                Some(hash),
            )
        }
        &_ => {
            panic!("command didn't match with any known.");
        }
    }
}

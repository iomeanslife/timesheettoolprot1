# timesheettoolprot1

An early prototype  for creating timesheet entries to a text file in the TOML 
format.

Missing
- Reading TOML DOM and always placing futures at the end of the file.
- Adding breaks/duration/end time.
- Parsing a configuration file with file path.
- Creating new files for months.
